// Includes relevant modules used by the QML
import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.20 as Kirigami

// Provides basic features needed for all kirigami applications
Kirigami.ApplicationWindow {
    // Unique identifier to reference this object
    id: root

    // Window title
    // i18nc() makes a string translatable
    // and provides additional context for the translators
    title: i18nc("@title:window", "Hello World")

    // Set the first page that will be loaded when the app opens
    // This can also be set to an id of a Kirigami.Page
    pageStack.initialPage: Kirigami.Page {
        actions.contextualActions: [
            Kirigami.Action {
                text: "Go actions"
                Kirigami.Action {
                    icon.name: "go-next"
                    text: "Next"
                }
                Kirigami.Action {
                    icon.name: "go-previous"
                    text: "Previous"
                }
            },
            Kirigami.Action {
                property bool isPlaying: false
                icon.name: isPlaying ? "media-playback-pause" : "media-playback-start"
                text: isPlaying ? "Pause" : "Play"
                onTriggered: isPlaying = !isPlaying
            }
        ]

        Column {
            anchors.fill: parent
            Controls.Button {
                icon.name: "help"
                text: "Press me!"
            }

            Controls.Label {
                // Center label horizontally and vertically within parent object
                text: i18n("Hello World!")
            }

            Controls.Slider {
                width: 100
            }
        }
    }
}
